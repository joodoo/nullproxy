#ifndef __NP_UTILS_H__
#define __NP_UTILS_H__

#include "NP.h"

int intf_index(int fd, char *interface);
struct Socket* create_socket(apr_pool_t *pool, SocketProto proto);
NP_IPAddr NP_inet_addr(char *ip);
NP_Port NP_htons(uint16_t);
char *NP_inet_ntoa(NP_IPAddr);
void NP_log_printf(const char *fmt, ...);
void NP_log_init();

#endif
