#ifndef __NP_UDP_H__
#define __NP_UDP_H__

#include "NP.h"
#include "NP_flow.h"

//int NP_udp_init(struct Socket *s, uint32_t ip, uint16_t port, char *intf);
NP_STATUS NP_udp_init(struct Socket *s);
NP_STATUS NP_udp_listen(struct Socket *s, NP_IPAddr ip, uint16_t port, char *intf);
struct Socket* NP_udp_accept(struct Socket *s);
NP_STATUS NP_udp_connect(struct Socket *s, NP_IPAddr ip, uint16_t port);
int NP_udp_read(struct Socket *s, uint8_t *buf, int len);
int NP_udp_write(struct Socket *s, uint8_t *buf, int len);
NP_STATUS NP_udp_close(struct Socket *s);

#endif

