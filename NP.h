#ifndef __NP_H__
#define __NP_H__

#include <sys/stat.h>
#include <sys/queue.h>
#include <unistd.h>
#include <sys/time.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <event.h>
#include <stdlib.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <linux/if.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <limits.h>
#include <linux/netfilter_ipv4.h>

#include "apr.h"
#include "apr_network_io.h"
#include "apr_strings.h"
#include "apr_queue.h"
#include "apr_hash.h"

#include "lwip/init.h"
#include "lwip/debug.h"
#include "lwip/mem.h"
#include "lwip/memp.h"
#include "lwip/sys.h"
#include "lwip/stats.h"
#include "lwip/ip.h"
#include "lwip/ip_frag.h"
#include "lwip/ip_addr.h"
#include "lwip/udp.h"
#include "lwip/tcp_impl.h"
#include "netif/etharp.h"

//GLOBALS
FILE *fp;
int nullProxyStatus;
apr_hash_t *HT;
apr_pool_t *mainpool;
int LOGLEVEL;

#define MAX_CLIENTS 100
#define BUFLEN 4096
#define IPV4 4
#define IPV6 16
#define LISTENQ 256

#define CRITICAL 0
#define ERROR    1
#define WARNING  2
#define INFO     3
#define DEBUG    4
#define MOREDEBUG    5

#define WHERESTR  "[%s:%d] [%s]: %5s"
#define WHEREARG  __FILE__, __LINE__, __func__, " "
#define DEBUGPRINT(...)         NP_log_printf(__VA_ARGS__);
#define Log2(_fmt, ...)  DEBUGPRINT(WHERESTR _fmt, WHEREARG, ##__VA_ARGS__)
#define Log(debug, message) if(debug <= LOGLEVEL) { Log2 message }

#define NP_IPAddr struct in_addr
#define NP_SockAddr struct sockaddr_in
#define NP_Port uint16_t

#define NP_STATUS int
#define NP_SUCCESS 0
#define NP_OK 0
#define NP_FAIL -1

typedef enum {
    NP_TCP,
    NP_UDP,
    NP_RAW,
    NP_TUN_TCP,
    NP_TUN_UDP,
    NP_TUN_LWIP,
    NP_NULL
} SocketProto;

typedef enum {
    NP_LISTEN,
    NP_CONNECT,
    NP_ACCEPT,
    NP_NOOP
} SocketType;

typedef enum {
    NP_LISTENING,
    NP_ACCEPTED,
    NP_CONNECTING,
    NP_CONNECTED,
    NP_NOOPSTATE
} SocketState;

typedef struct {
    NP_IPAddr ip;
    NP_Port port;
    SocketProto proto;
} NP_Host;


//Describes a flow. 1 is the read side, 2 is the write side
struct Flow {
    int key;
    int id;
    int next;
    struct Socket *sock1;
    struct Socket *sock2;
    apr_pool_t *pool;
    apr_queue_t *queue1;
    apr_queue_t *queue2;
    int (*process)(struct Flow*, int);
};


struct Buffer {
    uint8_t *data;
    uint32_t len;
};


struct Socket {
    SocketType type; 
    SocketProto proto; 
    uint32_t lip;       //local ip port
    uint16_t lport;
    uint32_t rip;       //remote ip port
    uint16_t rport;
    int fd;
    int fid; //flow this socket belongs too
    int interface;
    char intf_name[8];
    struct event *ev;
    void *addr;
    int addrlen;
    struct netif *netif;
    void *lwip_pcb;
    apr_queue_t *inQ;
    apr_queue_t *outQ;
    SocketState state;
    apr_pool_t *pool;
    NP_STATUS (*init)(struct Socket* s);
    NP_STATUS (*listen)(struct Socket*, NP_IPAddr ip, uint16_t port, char *interface);
    NP_STATUS (*connect)(struct Socket*, NP_IPAddr, uint16_t);
    struct Socket *(*accept)(struct Socket *s);
    int (*read)(struct Socket*, uint8_t*, int);
    int (*write)(struct Socket*, uint8_t*, int);
    NP_STATUS (*close)(struct Socket*);
    NP_STATUS (*fwd_chk)(struct Socket*);
    NP_STATUS (*enQ)(apr_queue_t*, struct Buffer*);
    struct Buffer *(*deQ)(apr_queue_t*);
};

struct TunInterface {
    char *ip;
    int fd;
};

struct Flow* create_flow();
struct Flow* set_flow(struct Flow *flow, struct Socket *s1, struct Socket *s2);
void process_flow(int fd, short event, void *arg);
struct Flow * forwarding(struct Socket *asock);
int protectFD(int fd);

void *NP_init(void*);
int nullProxyStart(char*, int);

#include "NP_utils.h"
#endif


