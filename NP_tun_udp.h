#ifndef __NP_TUN_UDP_H__
#define __NP_TUN_UDP_H__

int NP_tun_udp_write(struct Socket *s, uint8_t *buf, int len);
int NP_tun_udp_close(struct Socket *s);
NP_STATUS NP_tun_udp_init(struct Socket *s);
NP_STATUS NP_tun_udp_listen(struct Socket *s, NP_IPAddr ip, uint16_t port, char *intf);

#endif
