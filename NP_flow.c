#include "NP_flow.h"


static int free_fid;
static int num_flows;
static int max_flows;

struct Flow FlowTable[MAX_FLOWS];

int init_flows(int num)
{
    int i;
    max_flows = num;
    free_fid  = -1;
    struct Flow *flow;

    for (i = 0; i < num; i++) {
        flow = &FlowTable[i];
        //FlowTable[i].id = -1;
        //FlowTable[i].next = free_fid;
        flow->id = -1;
        flow->next = free_fid;
        apr_pool_create(&flow->pool, mainpool);
        //apr_queue_create(&flow->queue1, 1000, flow->pool);
        //apr_queue_create(&flow->queue2, 1000, flow->pool);
        free_fid = i;
    }

    num_flows = 0;

    return NP_OK;
}

struct Flow* get_flow(int id)
{
    return &FlowTable[id];
}

int new_flow()
{
    int ret = free_fid;
    FlowTable[free_fid].id = free_fid;
    free_fid = FlowTable[free_fid].next;

    num_flows++;
    return ret;
}

void free_flow(int id)
{
    //you cannot free a already freed flow
    assert(FlowTable[id].id != -1);
    FlowTable[id].id = -1;
    FlowTable[id].next = free_fid;
    free_fid = id;
    num_flows--;
    return;
}

void print_flows()
{
    int i;
    for (i = 0; i < max_flows; i++) {
        Log(INFO, ("%d -> %d\n", i, FlowTable[i].next));
    }

    i = max_flows-1;
    while (FlowTable[i].next != -1) {
        fprintf(stderr, "%d[%d]   ", i, FlowTable[i].id);
        i = FlowTable[i].next;
    }
    fprintf(stderr, "\nnum flows %d\n", num_flows);
}

void test_flows()
{
    int i;
    struct Flow *f;
    //, r;

    init_flows(10);

    for (i=0; i<3; i++) {
        f = get_flow(new_flow());
        Log(DEBUG, ("got flow %d\n", f->id));
    }

    Log(DEBUG, ("total flows after adding 10 = %d\n", num_flows));
    assert(num_flows == 3);

    print_flows();

    free_flow(8);
    free_flow(7);
    free_flow(9);
    //randomly remove a few flows
    /*
    srandom(time(NULL));
    for (i=0; i<2; i++) {
        r = (int)(random()%10);
        free_flow(r);
    }
    */

    print_flows();
}
