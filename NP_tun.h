#ifndef __NP_TUN_TCP_H__
#define __NP_TUN_TCP_H__

int NP_tun_read(struct Socket *s, uint8_t *buf, int len);
int NP_tun_write(struct Socket *s, uint8_t *buf, int len);
int NP_tun_close(struct Socket *s);
NP_STATUS NP_tun_init(struct Socket *s);
NP_STATUS NP_tun_listen(struct Socket *s, NP_IPAddr ip, uint16_t port, char *intf);
struct Socket* NP_tun_accept(struct Socket *s);
NP_STATUS NP_tun_connect(struct Socket *s, NP_IPAddr ip, uint16_t port);

struct pbuf * low_level_input(struct netif *netif);
err_t low_level_link_output(struct netif *netif, struct pbuf *p);
err_t low_level_output(struct netif *netif, struct pbuf *p, ip_addr_t *ip);
err_t tun_init(struct netif *netif);

#endif
