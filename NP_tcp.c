#include "NP_tcp.h"

NP_STATUS NP_tcp_init(struct Socket *s)
{
    //set the FPs
    s->listen = NP_tcp_listen;
    s->accept = NP_tcp_accept;
    s->connect = NP_tcp_connect;
    s->read = NP_tcp_read;
    s->write = NP_tcp_write;
    s->close = NP_tcp_close;
    s->deQ = NP_tcp_deQ;
    s->enQ = NP_tcp_enQ;
    return 0;
}

NP_STATUS NP_tcp_listen(struct Socket *sock, NP_IPAddr ip, uint16_t port, char *interface)
{
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = port;
    addr.sin_addr.s_addr = ip.s_addr;

    memcpy(sock->addr, &addr, sizeof(addr));
    sock->addrlen = sizeof(addr);
    
    sock->lport = port;
    sock->lip = ip.s_addr;

    if(interface != NULL) {
        sock->interface = intf_index(sock->fd, interface);
    }

    sock->type = NP_LISTEN;

    sock->fd = socket(PF_INET, SOCK_STREAM, 0);

    if (bind(sock->fd, (struct sockaddr *)sock->addr, sizeof(struct sockaddr_in)) == -1) {
        perror("bind failed");
        return NP_FAIL;
    }

    if (listen(sock->fd, LISTENQ) == -1) {
        perror("listen failed");
        Log(ERROR, ("Listen failed errno %d\n", errno));
        return NP_FAIL;
    }
    Log(INFO, ("Created LISTEN socket %p fd %d\n", sock, sock->fd));

    return NP_OK;
}

int NP_tcp_connect(struct Socket *s, NP_IPAddr ip, uint16_t port)
{
    int ret;
    struct sockaddr_in addr;

    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = port;
    addr.sin_addr.s_addr = ip.s_addr;

    s->rip = ip.s_addr;
    s->rport = port;

    s->fd = socket(PF_INET, SOCK_STREAM, 0);

    Log(INFO, ("Setting socket %d to non-blocking\n", s->fd));
    int flags = fcntl(s->fd, F_GETFL, 0);
    fcntl(s->fd, F_SETFL, flags | O_NONBLOCK);

    Log(INFO, ("Binding socket %d to %s\n", s->fd, s->intf_name));
    if (protectFD(s->fd) < 0) {
        Log(ERROR, ("protectFD failed for fd %d\n", s->fd));
        return -1;
    }

    Log(INFO, ("Connecting to ip %s:%d\n", NP_inet_ntoa(ip), NP_htons(port)));
    ret = connect(s->fd, (struct sockaddr*) &addr, sizeof(addr));

    if (ret < 0 && errno != EINPROGRESS) {
        perror("connect()");
        Log(ERROR, ("Connect failed errno %d\n", errno));
        return ret;
    }

    memcpy(s->addr, &addr, sizeof(struct sockaddr_in));

    s->state = NP_CONNECTING;

    Log(INFO, ("Created CONNECT socket %p fd %d\n", s, s->fd));
    return 0;
}

struct Socket* NP_tcp_accept(struct Socket *s)
{
    int fd;
    struct sockaddr_in remote, orig;
    socklen_t remotelen = sizeof(remote);
    socklen_t origlen = sizeof(orig);
    struct Socket *sock;

    if ((fd = accept(s->fd, (struct sockaddr*)&remote, &remotelen)) < 0) {
        Log(ERROR, ("Failed to accept connection on fd %d\n", s->fd));
        return NULL;
    }

    sock = create_socket(s->pool, NP_TCP);
    sock->rip = remote.sin_addr.s_addr;
    sock->rport = remote.sin_port;
    sock->fd = fd;
    memcpy(sock->addr, &remote, sizeof(struct sockaddr_in));
    sock->state = NP_ACCEPTED;

    if (0 == getsockopt(sock->fd, SOL_IP, SO_ORIGINAL_DST, (void *)&orig, &origlen)) {
        Log(INFO, ("remote dest 0x%x\n", orig.sin_addr.s_addr));
    }
    //local IP is the original dest. in case of transparent connection.
    //otherwise is the ip of this machine
    sock->lip = orig.sin_addr.s_addr;
    sock->lport = orig.sin_port;

    Log(INFO, ("Accepted connection on fd %d, from remote 0x%x:%x\n", 
                s->fd, sock->rip, sock->rport));
    return sock;
}

struct Buffer * NP_tcp_deQ(struct apr_queue_t *q)
{
    int ret;
    struct Buffer *buf;
    ret = apr_queue_trypop(q, (void*)&buf);
    if (ret == APR_EAGAIN) {
        return NULL;
    }
    return buf;
}

NP_STATUS NP_tcp_enQ(apr_queue_t *q, struct Buffer *buf)
{
    int ret;
    ret = apr_queue_trypush(q, buf);
    if(ret == APR_EAGAIN) {
        Log(ERROR, ("inQ full %d\n", apr_queue_size(q)));
        return NP_FAIL;
    }
    else {
        Log(INFO, ("inQ enqueued buflen %d qsize %d\n", buf->len, apr_queue_size(q)));
        return NP_SUCCESS;
    }
}

int NP_tcp_read(struct Socket *s, uint8_t *buftmp, int len)
{
    struct Buffer *buf;
    
    buf = apr_pcalloc(s->pool, sizeof(struct Buffer));
    buf->data = apr_pcalloc(s->pool, BUFLEN);

    buf->len = read(s->fd, buf->data, BUFLEN);

    s->enQ(s->inQ, buf);

    Log(DEBUG, ("NP_tcp_read: sock %p read len %d fd %d\n", s, buf->len, s->fd));
    return buf->len;
}

int NP_tcp_write(struct Socket *s, uint8_t *buft, int len)
{
    int ret, l;
    struct Buffer *buf;
    ret = apr_queue_trypop(s->outQ, (void*)&buf);

    if(ret == APR_EAGAIN) {
        Log(ERROR, ("s->outQ empty %d\n", apr_queue_size(s->outQ)));
        return 0;
    }
    else {
        Log(INFO, ("s->outQ dequeued %d\n", apr_queue_size(s->outQ)));
    }

    l = write(s->fd, buf->data, buf->len);
    Log(DEBUG, ("sock %p wrote len %d fd %d\n", s, l, s->fd));
    return l;
}

int NP_tcp_close(struct Socket *s)
{
    Log(DEBUG, ("closing sock %p fd %d\n", s, s->fd));
    return close(s->fd);
}

struct Socket* create_tcp_socket(apr_pool_t *pool, uint32_t ip, uint16_t port, char *interface, SocketType type)
{
    struct Socket *sock;
    sock = create_socket(pool, NP_TCP);
    sock->type = type;
    //for connect ip and port are for destination
    //for listen ip and port are for src
    sock->lport = port;
    sock->lip = ip;

    struct sockaddr_in addr;

    if(sock->type == NP_ACCEPT || sock->type == NP_NOOP) {
        Log(INFO, ("Created ACCEPT socket %p \n", sock));
        return sock;
    }

    sock->fd = socket(PF_INET, SOCK_STREAM, 0);

    if(interface != NULL) {
        sock->interface = intf_index(sock->fd, interface);
    }

    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = port;
    addr.sin_addr.s_addr = ip;

    //set functions
    sock->read = NP_tcp_read;
    sock->write = NP_tcp_write;
    sock->close = NP_tcp_close;
    sock->accept = NP_tcp_accept;
    sock->connect = NP_tcp_connect;

    if(sock->type == NP_LISTEN) {

        if (bind(sock->fd, (struct sockaddr *) &addr, sizeof(struct sockaddr_in)) == -1) {
            perror("bind failed");
            return NULL;
        }
        if (listen(sock->fd, LISTENQ) == -1) {
            perror("listen failed");
            Log(ERROR, ("Listen failed errno %d\n", errno));
            return NULL;
        }
        Log(INFO, ("Created LISTEN socket %p fd %d\n", sock, sock->fd));
    }
    else if(sock->type == NP_CONNECT) {
        if (connect(sock->fd, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
            perror("connect()");
            Log(ERROR, ("Connect failed errno %d\n", errno));
            return NULL;
        }
        Log(INFO, ("Created CONNECT socket %p fd %d\n", sock, sock->fd));
    }
    else {
        Log(INFO, ("No type specified %d\n", sock->type));
    }

    return sock;
}
