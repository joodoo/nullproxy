ARCH=linux

AND_TC=~/android-toolchain/arm-linux-androideabi-4.4.3

ifeq ($(ARCH),android)
	CC=$(AND_TC)/bin/arm-linux-androideabi-gcc
	CXX=$(AND_TC)/bin/arm-linux-androideabi-g++
	HOST=--host=arm-linux-androideabi
else
	CC=gcc
	CXX=g++
	HOST=""
endif

$(shell mkdir -p prebuilt/$(ARCH))
$(shell mkdir -p build-$(ARCH))

OBJS=main.o nullproxy.o NP_utils.o NP_tcp.o NP_tun.o NP_udp.o NP_tun_udp.o NP_flow.o NP_config.o parson.o

BUILD=$(CURDIR)/build-$(ARCH)
LDFLAGS=-L$(CURDIR)/prebuilt/$(ARCH) -L.
ifeq ($(ARCH),android)
LIBS=-levent -laprutil-1 -lapr-1 -llwip4 -lexpat -lm
else
LIBS=-levent -laprutil-1 -lapr-1 -llwip4 -lpthread -lrt
endif
CFLAGS=-Wall -Werror -g -D__64BIT__ -D_GNU_SOURCE -D_REENTRANT -DIPv4 -fpack-struct -DLWIP_DEBUG -I$(BUILD)/usr/local/include -D_LARGEFILE64_SOURCE -I$(BUILD)/usr/local/apr/include/apr-1

LIBEVENT=libevent-2.0.16-stable
LIBAPR=apr-1.4.2
LIBAPRUTIL=apr-util-1.3.10

LWIPDIR=lwip/src

CFLAGS:=$(CFLAGS) -I$(LWIPDIR)/include -I$(LWIPDIR)/include/ipv4 -I. -D_$(ARCH)_

# COREFILES, CORE4FILES: The minimum set of files needed for lwIP.
COREFILES=$(LWIPDIR)/core/mem.c $(LWIPDIR)/core/memp.c $(LWIPDIR)/core/netif.c \
  $(LWIPDIR)/core/pbuf.c $(LWIPDIR)/core/raw.c \
  $(LWIPDIR)/core/stats.c $(LWIPDIR)/core/sys.c \
        $(LWIPDIR)/core/tcp.c $(LWIPDIR)/core/tcp_in.c \
        $(LWIPDIR)/core/tcp_out.c $(LWIPDIR)/core/udp.c $(LWIPDIR)/core/dhcp.c \
  $(LWIPDIR)/core/init.c $(LWIPDIR)/core/timers.c $(LWIPDIR)/core/def.c
CORE4FILES=$(LWIPDIR)/core/ipv4/icmp.c $(LWIPDIR)/core/ipv4/ip.c \
  $(LWIPDIR)/core/ipv4/inet.c $(LWIPDIR)/core/ipv4/ip_addr.c \
  $(LWIPDIR)/core/ipv4/ip_frag.c $(LWIPDIR)/core/ipv4/inet_chksum.c

# NETIFFILES: Files implementing various generic network interface functions.'
NETIFFILES=$(LWIPDIR)/netif/etharp.c

# ARCHFILES: Architecture specific files
ARCHFILES=$(LWIPDIR)/sys_arch.c

# LWIPFILES: All the above.
LWIPFILES=$(COREFILES) $(CORE4FILES) $(NETIFFILES) $(ARCHFILES)
LWIPFILESW=$(wildcard $(LWIPFILES))
LWIPOBJS=$(LWIPFILESW:.c=.o)

%.o: %.c
	@echo "compiling obj $<"
	$(CC) -fPIC -c -o $@ $< $(CFLAGS)



all: driver libnullproxy.so



libevent: prebuilt/$(ARCH)/libevent.a

$(BUILD)/$(LIBEVENT)/configure:
	cd libs && tar -zxf libevent-2.0.16-stable.tar.gz -C $(BUILD)

$(BUILD)/$(LIBEVENT)/Makefile: $(BUILD)/$(LIBEVENT)/configure
	@echo "Running libevent configure"
ifeq ($(ARCH),android)
	$(shell cp libs/config.* $(BUILD)/$(LIBEVENT)/)
	cd $(BUILD)/$(LIBEVENT) && CC=$(CC) CXX=$(CXX) ./configure --host=arm-linux-androideabi --with-pic
else
	cd $(BUILD)/$(LIBEVENT) && CC=$(CC) CXX=$(CXX) ./configure --with-pic
endif

$(BUILD)/usr/local/lib/libevent.a: $(BUILD)/$(LIBEVENT)/Makefile
	cd $(BUILD)/$(LIBEVENT) && make DESTDIR=$(BUILD) install

$(BUILD)/$(LIBEVENT)/.libs/libevent.a: $(BUILD)/$(LIBEVENT)/Makefile
	cd $(BUILD)/$(LIBEVENT) && make && make DESTDIR=$(BUILD) install

prebuilt/$(ARCH)/libevent.a: $(BUILD)/$(LIBEVENT)/.libs/libevent.a
	cp $< $@





#apr
libapr: prebuilt/$(ARCH)/libapr-1.a

$(BUILD)/$(LIBAPR)/configure:
	cd libs && tar -zxf apr-1.4.2.tar.gz -C $(BUILD)

$(BUILD)/$(LIBAPR)/Makefile: $(BUILD)/$(LIBAPR)/configure
	@echo "Running libapr configure"
ifeq ($(ARCH),android)
	$(shell cp libs/config.* $(BUILD)/$(LIBAPR)/build)
	cd $(BUILD)/$(LIBAPR) && CC=$(CC) CXX=$(CXX) ./configure --host=arm-linux-androideabi ac_cv_func_setpgrp_void=yes ac_cv_file__dev_zero=yes apr_cv_tcp_nodelay_with_cork=yes apr_cv_process_shared_works=yes ac_cv_sizeof_struct_iovec=8 apr_cv_mutex_recursive=yes apr_cv_mutex_robust_shared=no --with-pic
else
	cd $(BUILD)/$(LIBAPR) && CC=$(CC) CXX=$(CXX) ./configure --with-pic
endif

$(BUILD)/$(LIBAPR)/.libs/libapr-1.a: $(BUILD)/$(LIBAPR)/Makefile
	cd $(BUILD)/$(LIBAPR) && make && make DESTDIR=$(BUILD) install

prebuilt/$(ARCH)/libapr-1.a: $(BUILD)/$(LIBAPR)/.libs/libapr-1.a
	cp $< $@





#apr-utils
libaprutil: libapr prebuilt/$(ARCH)/libaprutil-1.a

$(BUILD)/$(LIBAPRUTIL)/configure:
	cd libs && tar -zxvf apr-util-1.3.10.tar.gz -C $(BUILD)

$(BUILD)/$(LIBAPRUTIL)/Makefile: $(BUILD)/$(LIBAPRUTIL)/configure
	@echo "Running libaprutil configure"
	$(shell cp libs/Makefile.in.expat $(BUILD)/$(LIBAPRUTIL)/xml/expat/Makefile.in)
	$(shell cp libs/config.* $(BUILD)/$(LIBAPRUTIL)/build)
	cd $(BUILD)/$(LIBAPRUTIL)/xml/expat && CC=$(CC) CXX=$(CXX) ./configure $(HOST) --with-apr=../$(LIBAPR) --with-pic
	cd $(BUILD)/$(LIBAPRUTIL)/xml/expat && make
	cd $(BUILD)/$(LIBAPRUTIL) && CC=$(CC) CXX=$(CXX) ./configure $(HOST) --with-apr=../$(LIBAPR) --with-pic

$(BUILD)/$(LIBAPRUTIL)/.libs/libaprutil-1.a: $(BUILD)/$(LIBAPRUTIL)/Makefile
	cd $(BUILD)/$(LIBAPRUTIL) && make && make DESTDIR=$(BUILD) install

prebuilt/$(ARCH)/libaprutil-1.a: $(BUILD)/$(LIBAPRUTIL)/.libs/libaprutil-1.a
	cp $(BUILD)/$(LIBAPRUTIL)/xml/expat/.libs/libexpat.so.0 prebuilt/$(ARCH)
	cp $(BUILD)/$(LIBAPRUTIL)/xml/expat/.libs/libexpat.a prebuilt/$(ARCH)
	cp $< $@





#lwip
liblwip4: prebuilt/$(ARCH)/liblwip4.a

$(BUILD)/liblwip4.a: $(LWIPOBJS)
	$(AR) $(ARFLAGS) $@ $?

prebuilt/$(ARCH)/liblwip4.a: $(BUILD)/liblwip4.a
	cp $< $@






driver: libnullproxy.so main.o driver.o
ifeq ($(ARCH),android)
	@echo "Skipping android binary build $@"
else
	$(CC) -g -o $@ main.o driver.o $(LDFLAGS) $(LIBS) -lnullproxy
endif
  

test: libnullproxy.so test.o
	$(CC) -g -o $@ test.o $(LDFLAGS) $(LIBS) -lnullproxy

nullproxy: liblwip4 libevent libapr libaprutil $(OBJS)
ifeq ($(ARCH),android)
#	$(CC) -g -o $(@) $(OBJS) $(LDFLAGS) $(LIBS) -Wl,-rpath,prebuilt/$(ARCH)
	@echo "Skipping android binary build $@"
else
	$(CC) -g -o $@ $(OBJS) $(LDFLAGS) $(LIBS)
endif

libnullproxy.so: liblwip4 libevent libapr libaprutil $(OBJS)
ifeq ($(ARCH),android)
	$(CC) -shared -g -o $(@) $(OBJS) $(LDFLAGS) $(LIBS) -Wl,-rpath,prebuilt/$(ARCH)
else
	$(CC) -shared -g -o $@ $(OBJS) $(LDFLAGS) $(LIBS)
endif

install:
	cp nullproxy \
	$(BUILD)/usr/local/lib/libevent-2.0.so.5 \
	$(BUILD)/usr/local/apr/lib/libapr-1.so.0 \
	prebuilt/$(ARCH)/libexpat.so.0 \
	$(BUILD)/usr/local/apr/lib/libaprutil-1.so.0 $(DESTDIR)

clean:
	rm -rf driver nullproxy libnullproxy.so $(OBJS) $(LWIPOBJS) driver.o main.o

distclean: clean
	cd libs/$(LIBEVENT) && make distclean
	cd libs/$(LIBAPR) && make distclean
	cd libs/$(LIBAPRUTIL) && make distclean
	rm -rf $(BUILD)
