/*
 * Compile with:
 * gcc -I install_d/usr/local/include -g3 -o nullproxy nullproxy.c -L. -levent -L. -lhash
 */

#include "NP.h"
#include "NP_utils.h"
#include "NP_flow.h"
#include "NP_tcp.h"
#include "NP_tun.h"
#include "NP_config.h"

//prototype
void NP_tcp_process_listen(int fd, short event, void *arg);
void NP_udp_process_listen(int fd, short event, void *arg);

void process_flow(int fd, short event, void *arg)
{
    int len;
    struct Buffer *b;
    struct Flow *flow;
    flow = (struct Flow*)arg;

    if (event & EV_READ) {
        if (fd == flow->sock1->fd) {
            Log(DEBUG, ("Read event on sock1 %p fd %d\n", flow->sock1, fd));
            len = flow->sock1->read(flow->sock1, NULL, BUFLEN);
            if (len < 0) {
                Log(ERROR, ("Error reading from sock1 %p fd %d\n", flow->sock1, fd));
            }
            else if (len == 0) {
                Log(INFO, ("sock1 closed connection, sock %p fd %d\n", flow->sock1, fd));
                flow->sock1->close(flow->sock1);
                //shutdown read/writes and 
                event_del(flow->sock1->ev);
                //drain the outQ of sock2
                if (apr_queue_size(flow->sock2->outQ) > 0) {
                    flow->sock2->write(flow->sock2, NULL, 0);
                }
                flow->sock2->close(flow->sock2);
            }
            else {
                b = flow->sock1->deQ(flow->sock1->inQ);
                Log(INFO, ("deQ sock1 len %d\n", b->len));
                flow->sock2->enQ(flow->sock2->outQ, b);
                if (flow->sock2->state == NP_CONNECTED) {
                    flow->sock2->write(flow->sock2, NULL, len);
                }
            }
        }
        if (fd == flow->sock2->fd) {
            Log(DEBUG, ("Read event on sock2 %p fd %d\n", flow->sock2, fd));
            len = flow->sock2->read(flow->sock2, NULL, BUFLEN);
            if (len < 0) {
                Log(ERROR, ("Error reading from sock2 %p fd %d\n", flow->sock2, fd));
            }
            else if (len == 0) {
                Log(INFO, ("sock2 closed connection, sock %p fd %d\n", flow->sock2, fd));
                flow->sock2->close(flow->sock2);
                event_del(flow->sock2->ev);
                if (apr_queue_size(flow->sock1->outQ) > 0) {
                    flow->sock1->write(flow->sock1, NULL, 0);
                }
                flow->sock1->close(flow->sock1);
            }
            else {
                b = flow->sock2->deQ(flow->sock2->inQ);
                Log(INFO, ("deQ sock2 len %d\n", b->len));
                flow->sock1->enQ(flow->sock1->outQ, b);
                flow->sock1->write(flow->sock1, NULL, len);
            }
        }
    }
    if (event & EV_WRITE) {
        Log(INFO, ("Write event, fd %d\n", fd));
        if (fd == flow->sock1->fd) {
            Log(INFO, ("Write event, Drain outQ on sock1 %p fd %d\n", flow->sock1, fd));
            flow->sock1->write(flow->sock1, NULL, 0);
        }
        if (fd == flow->sock2->fd) {
            Log(INFO, ("Write event, Drain outQ on sock2 %p fd %d\n", flow->sock2, fd));
            flow->sock2->write(flow->sock2, NULL, 0);
        }
    }
    if (event & EV_TIMEOUT) {
        if (fd == flow->sock1->fd) {
            Log(DEBUG, ("Timeout event on sock %p fd %d\n", flow->sock1, fd));
        }
        if (fd == flow->sock2->fd) {
            Log(DEBUG, ("Timeout event on sock %p fd %d\n", flow->sock2, fd));
        }
    }
}

struct Flow* set_flow(struct Flow *flow, struct Socket *s1, struct Socket *s2)
{
    flow->sock1 = s1;
    flow->sock2 = s2;
    return flow;
}


void socket_connected(int fd, short event, void *arg)
{
    struct Socket *s1, *s2;
    struct Flow *flow = (struct Flow*)arg;
    s1 = flow->sock1; //accepted socket
    s2 = flow->sock2; //connected socket
    s2->state = NP_CONNECTED;

    NP_IPAddr ip0, ip1;
    ip0.s_addr = s1->rip;
    ip1.s_addr = s2->rip;

    Log(INFO, ("Flow id %d ready to go between 0x%x:%x <--> 0x%x:%x\n", flow->id, 
                NP_inet_ntoa(ip0), s1->rport,
                NP_inet_ntoa(ip1), s2->rport));

    event_set(s2->ev, s2->fd, EV_READ|EV_PERSIST, process_flow, flow);
    event_add(s2->ev, NULL);

    process_flow(fd, event, arg);
}


//compare the dest ip to the rules and make decision to 
//forward or proxy the connection
NP_Host *evaluate_rule(struct Socket *asock)
{
    NP_Host *host;
    host = apr_pcalloc(asock->pool, sizeof(NP_Host));
    char target[32];
    char *dest;
    char proto[8];
    char dport[8];
    NP_IPAddr tmp;

    tmp.s_addr = asock->lip;

    //get the asock parameters to match with 
    //the text version of config file
    dest = NP_inet_ntoa(tmp);
    sprintf(dport ,"%d", asock->lport);
    if (asock->proto == NP_TUN_TCP) {
      strcpy(proto, "tcp");
    }
    else if (asock->proto == NP_TUN_UDP) {
      strcpy(proto, "udp");
    }

    int i = 0;
    for (i=0; i<NP_CFG->rule_cnt; i++) {
      if ((0 == strcmp(NP_CFG->rules[i].dest, "any") || 
          0 == strcmp(NP_CFG->rules[i].dest, dest)) &&
          (0 == strcmp(NP_CFG->rules[i].proto, "any") || 
          0 == strcmp(NP_CFG->rules[i].proto, proto))) {
          strcpy(target, NP_CFG->rules[i].target);
          break;
      }
    }

    //in forward mode connect to client specified IP
    if (0 == strcmp(target, "forward")) {
        host->ip.s_addr = asock->lip;
        host->port  = asock->lport;
        if (asock->proto == NP_TUN_TCP) {
            host->proto = NP_TCP;
        }
        else {
            host->proto = NP_UDP;
        }
    }
    else if (0 == strcmp(target, "proxy")) {
        NP_IPAddr tmp = NP_inet_addr(NP_CFG->rules[i].proxies[0].ip);
        host->ip.s_addr = tmp.s_addr; 
        host->port = NP_CFG->rules[i].proxies[0].port;
    }

    return host;
}


//called one time to decide what do to with the data coming in
//on the accepted Socket
// forward it to the client requested dest IP:Port
// OR proxy it to a fixed IP:Port, depending on the config rules
struct Flow * forwarding(struct Socket *asock)
{
    struct Socket *csock;
    struct Flow *flow;
    NP_Host *host;

    Log(ERROR, ("Starting forwarding decision for accepted socket, remote 0x%x:%x, local 0x%x:%x\n", 
                  asock->rip, asock->rport, asock->lip, asock->lport));

    host = evaluate_rule(asock);

    csock = create_socket(mainpool, host->proto);
    strcpy(csock->intf_name, NP_CFG->out_intf);

    if (csock == NULL) {
        Log(ERROR, ("cannot create connect socket to 0x%x\n", 0));
    }
    else {
        if (csock->connect(csock, host->ip, NP_htons(host->port)) < 0) {
            Log(ERROR, ("ERROR Connecting to remote dest 0x%x:%x\n", host->ip.s_addr, NP_htons(8080)));
            return NULL;
        }

        asock->fid = new_flow();
        flow = get_flow(asock->fid);
        set_flow(flow, asock, csock);

        NP_IPAddr ip0, ip1, ip2, ip3;
        ip0.s_addr = asock->lip;
        ip1.s_addr = asock->rip;
        ip2.s_addr = csock->lip;
        ip3.s_addr = csock->rip;
        Log(INFO, ("Creating flow id %d between (%s:%d %s:%d) <-- \n", flow->id, 
                    NP_inet_ntoa(ip1), NP_htons(asock->rport),
                    NP_inet_ntoa(ip0), NP_htons(asock->lport)));

        Log(INFO, ("Creating flow id %d between --> (%s:%d %s:%d)\n", flow->id, 
                    NP_inet_ntoa(ip2), NP_htons(csock->lport),
                    NP_inet_ntoa(ip3), NP_htons(csock->rport)));

        if (host->proto == NP_UDP) {
            //for UDP no need to wait for the socket to be connected 
            socket_connected(csock->fd, EV_WRITE, flow);
        }
        else {
            event_set(csock->ev, csock->fd, EV_WRITE, socket_connected, flow);
            event_add(csock->ev, NULL);
        }
    }

    return flow;
}


void NP_tcp_process_listen(int fd, short event, void *arg)
{
    struct Socket *sock = (struct Socket*)arg;
    struct Socket *asock;
    struct Flow *flow;

    if (fd != sock->fd) {
        Log(ERROR, ("Something is wrong, fd cannot be different from the opaque data fd (%d != %d)\n", fd, sock->fd));
    }

    if (event == EV_READ) {
        Log(INFO, ("Accept the connection on listen fd %d\n", sock->fd));

        asock = sock->accept(sock);
        if (asock == NULL) {
            Log(ERROR, ("Failed to accept connection on fd %d\n", sock->fd));
            return;
        }

        flow = forwarding(asock);

        event_set(asock->ev, asock->fd, EV_READ|EV_PERSIST, process_flow, flow);
        event_add(asock->ev, NULL);
    }
}


void NP_udp_process_listen(int fd, short event, void *arg)
{
    struct Socket *lsock = (struct Socket*)arg;
    struct Socket *asock;
    struct Flow *flow = NULL;

    if (fd != lsock->fd) {
        Log(ERROR, ("Something is wrong, fd cannot be different from the opaque data fd (%d != %d)\n", fd, lsock->fd));
    }

    if (event == EV_READ) {
        Log(INFO, ("Accept the connection on listen fd %d\n", lsock->fd));

        asock = lsock->accept(lsock);
        if (asock == NULL) {
            Log(ERROR, ("Failed to accept connection on fd %d\n", lsock->fd));
            return;
        }

        //for UDP we dont know if the pkt in is for a new connected or old one. So
        //our accept call in UDP looks for any existing sockets in the HT and sends it here
        if(asock->state == NP_CONNECTED) {
            flow = get_flow(asock->fid);
        }
        else if(asock->state == NP_ACCEPTED) {
            flow = forwarding(asock);
        }

        process_flow(asock->fd, EV_READ, flow);
    }
}


void NP_tun_process_listen(int fd, short event, void *arg)
{
    struct Socket *lsock = (struct Socket*)arg;

    if (fd != lsock->fd) {
        Log(ERROR, ("Something is wrong, fd cannot be different from the opaque data fd (%d != %d)\n", 
                                  fd, lsock->fd));
    }

    if (event == EV_READ) {
        Log(INFO, ("Read data on listen fd %d\n", lsock->fd));

        lsock->accept(lsock);
        //asock is created in tun_accept CB, so this is always NULL here.
    }
}


void timer_cb(int i, short s, void *v)
{

    long time = (long)v;
    //Log(DEBUG, ("Firing tcp_tmr %lu\n", (long)v));
    tcp_tmr();

    struct event *timeout;
    struct timeval tv;

    timeout = calloc(1, sizeof(struct event));

    evtimer_set(timeout, timer_cb, v);

    evutil_timerclear(&tv);
    tv.tv_sec  += (time / 1000);
    tv.tv_usec += ((time % 1000) * 1000);

    evtimer_add(timeout, &tv);
}

void health_check_timer(int i, short s, void *v)
{
    long time = (long)v;
    Log(DEBUG, ("Health check status %d, timer %lu ms\n", nullProxyStatus, time));
    printf("Health check timer %lu\n", time);

    struct event *timeout;
    struct timeval tv;

    timeout = calloc(1, sizeof(struct event));

    evtimer_set(timeout, health_check_timer, v);

    evutil_timerclear(&tv);
    tv.tv_sec  += (time / 1000);
    tv.tv_usec += ((time % 1000) * 1000);

    evtimer_add(timeout, &tv);
}


/*
static int load_config()
{
    NP_CFG = apr_pcalloc(mainpool, sizeof(struct Config));
    struct Config TEST_CFG = { 
        .rules = {
            {
                .src = "any", 
                .sport = 0, 
                .dest = "any", 
                .dport = 0, 
                .proto = "any",
                .target = "forward",
                .proxies = {
                    {.ip = "12.0.71.180", .port = 8080},
                    {.ip = "", .port = 0}
                }
            },
            {
                .src = "any", 
                .sport = 0, 
                .dest = "", 
                .dport = 0, 
                .proto = "any",
                .target = "", 
                .proxies = {
                    {.ip = "", .port = 0},
                    {.ip = "", .port = 0}
                }
            }
        },
        .out_intf = "eth0", // only used for linux
        .logfile = "/tmp/nullproxy.log",
        .loglevel = DEBUG
    };

    memcpy(NP_CFG, (void*)&TEST_CFG, sizeof(TEST_CFG));

    return 0;
}
*/

void *NP_init(void *arg)
{
    struct Socket *tcp_lsock;
    struct Socket *udp_lsock;
    struct Socket *tun_lsock;
    struct Socket *tun_udp_lsock;

    //XXX special case because for android the interface is created in Java
    //space so we are replicating that for linux as well, otherwise there
    //is not need to get the interface using args like here.
    struct TunInterface *ti = (struct TunInterface*)arg;

    apr_initialize();
    atexit(apr_terminate);
    apr_pool_create(&mainpool, NULL);
    lwip_init();

    //load config
    //load_config();
    load_config2();

    NP_log_init();

    Log(INFO, ("Starting the application %d\n", 0));
    Log(INFO, ("Starting the application tun ip %s fd %d\n", ti->ip, ti->fd));


    HT = apr_hash_make(mainpool);

    tcp_lsock = create_socket(mainpool, NP_TCP);
    if(tcp_lsock == NULL) {
        Log(ERROR, ("Cannot create listening socket %d\n", errno));
        exit(-1);
    }
    tcp_lsock->listen(tcp_lsock, NP_inet_addr("0.0.0.0"), htons(4444), "eth1");
    Log(INFO, ("Starting TCP socket fd %d\n", tcp_lsock->fd));

    udp_lsock = create_socket(mainpool, NP_UDP);
    if(udp_lsock == NULL) {
        Log(ERROR, ("Cannot create listening socket %d\n", errno));
        exit(-1);
    }
    udp_lsock->listen(udp_lsock, NP_inet_addr("0.0.0.0"), htons(4445), "eth1");
    Log(INFO, ("Starting UDP listen socket fd %d\n", udp_lsock->fd));

    tun_lsock = create_socket(mainpool, NP_TUN_TCP);
    if(tun_lsock == NULL) {
        Log(ERROR, ("Cannot create tuntap listening socket %d\n", errno));
        exit(-1);
    }
    tun_lsock->listen(tun_lsock, NP_inet_addr(ti->ip), htons(80), "tun2");
    tun_lsock->fd = ti->fd;
    Log(INFO, ("Starting TUN TCP listen socket for port 80, fd %d\n", tun_lsock->fd));

    tun_udp_lsock = create_socket(mainpool, NP_TUN_UDP);
    if(tun_udp_lsock == NULL) {
        Log(ERROR, ("Cannot create tuntap listening socket %d\n", errno));
        exit(-1);
    }
    tun_udp_lsock->listen(tun_udp_lsock, NP_inet_addr(ti->ip), htons(53), "tun2");
    tun_udp_lsock->fd = ti->fd;
    Log(INFO, ("Starting TUN UDP listen socket for port 53, fd %d\n", tun_udp_lsock->fd));

    event_init();

    test_flows();

    //add the socket to libevent and make it PERSIST
    event_set(tcp_lsock->ev, tcp_lsock->fd, EV_READ|EV_PERSIST, NP_tcp_process_listen, tcp_lsock);
    event_add(tcp_lsock->ev, NULL);

    event_set(udp_lsock->ev, udp_lsock->fd, EV_READ|EV_PERSIST, NP_udp_process_listen, udp_lsock);
    event_add(udp_lsock->ev, NULL);

    event_set(tun_lsock->ev, tun_lsock->fd, EV_READ|EV_PERSIST, NP_tun_process_listen, tun_lsock);
    event_add(tun_lsock->ev, NULL);

    event_set(tun_udp_lsock->ev, tun_udp_lsock->fd, EV_READ|EV_PERSIST, NP_tun_process_listen, tun_udp_lsock);
    event_add(tun_udp_lsock->ev, NULL);

    timer_cb(-1, 0, (void*)250);
    health_check_timer(-1, 0, (void*)5000);

    Log(INFO, ("Before dispatch %d\n", 0));
    event_dispatch();
    Log(INFO, ("After dispatch %d\n", 0));
    return 0;
}
