#ifndef __NP_TCP_H__
#define __NP_TCP_H__

#include "NP.h"
#include "NP_flow.h"
#include "NP_utils.h"

//int NP_tcp_init(struct Socket *s, uint32_t ip, uint16_t port, char *intf);
int NP_tcp_init(struct Socket *s);
int NP_tcp_listen(struct Socket *s, NP_IPAddr ip, uint16_t port, char *intf);
struct Socket*  NP_tcp_accept(struct Socket *s);
int NP_tcp_connect(struct Socket *s, NP_IPAddr ip, uint16_t port);
int NP_tcp_read(struct Socket *s, uint8_t *buf, int len);
int NP_tcp_write(struct Socket *s, uint8_t *buf, int len);
int NP_tcp_close(struct Socket *s);
struct Buffer* NP_tcp_deQ(apr_queue_t *q);
int NP_tcp_enQ(apr_queue_t *q, struct Buffer *b);

struct Socket* create_tcp_socket(apr_pool_t *pool, uint32_t ip, uint16_t port, char *interface, SocketType type);

#endif
