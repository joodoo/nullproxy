#include "NP.h"
#include <linux/if.h>
#include <linux/if_tun.h>
#define DEVTAP "/dev/net/tun"

int get_tun_interface()
{
    int fd;

    fd = open(DEVTAP, O_RDWR);
    if (fd == -1) {
        perror("tunif: tunif_init: open");
        return fd;
    }

    printf("Setting fd %d to non-blocking\n", fd);
    int flags = fcntl(fd, F_GETFL, 0);
    fcntl(fd, F_SETFL, flags | O_NONBLOCK);

    struct ifreq ifr;
    memset(&ifr, 0, sizeof(ifr));
    ifr.ifr_flags = IFF_TUN|IFF_NO_PI;
    strncpy(ifr.ifr_name, "tun2", IFNAMSIZ);

    if (ioctl(fd, TUNSETIFF, (void *) &ifr) < 0) {
        perror("ioctl failed");
        exit(1);
    }

    return fd;
}


int main(int argc, char **argv)
{
    char buf[1024];
    char ip[16];
    
    int fd = get_tun_interface();

    bzero(buf, 1024);
    if (argc == 1) {
        strcpy(ip, "192.168.200.200");
    }
    else {
        strcpy(ip, argv[1]);
    }

    snprintf(buf, sizeof(buf), "/sbin/ifconfig tun2 inet %s netmask 255.255.255.0", ip);
    system(buf);

    bzero(buf, 1024);
    snprintf(buf, sizeof(buf), "/sbin/ip route add 54.241.22.205/32 via %s dev tun2", ip);
    system(buf);
    snprintf(buf, sizeof(buf), "/sbin/ip route add 8.8.8.8/32 via %s dev tun2", ip);
    system(buf);

    nullProxyStart(ip, fd);
    return 0;
}
