#include "NP.h"
#include "NP_tun.h"
#include "NP_tcp.h"
#include "NP_tun_udp.h"
#include <linux/if.h>
#include <linux/if_tun.h>

NP_STATUS tun_input(struct Socket *s);

//CBs for lwip
void  tun_udp_recv(void *arg, struct udp_pcb *pcb, struct pbuf *p, ip_addr_t *addr, u16_t port);


NP_STATUS NP_tun_udp_init(struct Socket *s)
{
    //set the FPs
    s->listen = NP_tun_udp_listen;
    s->accept = NP_tun_accept;
    s->connect = NP_tun_connect;
    s->read = NP_tun_read;
    s->write = NP_tun_udp_write;
    s->close = NP_tun_udp_close;
    s->deQ = NP_tcp_deQ;
    s->enQ = NP_tcp_enQ;

    return 0;
}


int NP_tun_udp_write(struct Socket *s, uint8_t *buft, int len)
{
    int ret;
    struct Buffer *buf;
    static int tlen = 0;
    struct pbuf *p;
    struct ip_addr dest;

    tlen = len;

    ret = apr_queue_trypop(s->outQ, (void*)&buf);

    if(ret == APR_EAGAIN) {
        Log(ERROR, ("s->outQ empty %d\n", apr_queue_size(s->outQ)));
        return 0;
    }
    else {
        Log(INFO, ("s->outQ dequeued %d\n", apr_queue_size(s->outQ)));
    }

    p = pbuf_alloc(PBUF_TRANSPORT, tlen+8, PBUF_RAM); 
    p->payload = buf->data; 

    dest.addr = s->rip;
    ret = udp_sendto(s->lwip_pcb, p, &dest, s->rport);

    return tlen;
}

int NP_tun_udp_close(struct Socket *s)
{
    Log(DEBUG, ("Close tun udp sock %p fd %d\n", s, s->fd));
    return 0;
}


NP_STATUS NP_tun_udp_listen(struct Socket *sock, NP_IPAddr ip, uint16_t port, char *intf)
{
    sock->lport = port;
    sock->lip = ip.s_addr;
    sock->interface = 0;
    sock->type = NP_LISTEN;
    sock->state = NP_LISTENING;

    struct netif *netif;
    static ip_addr_t ipaddr, netmask, gw;

    ipaddr.addr = ip.s_addr;
    gw.addr = 0;
    netmask.addr = 0;

    sock->netif = apr_pcalloc(sock->pool, sizeof(struct netif));

    netif = sock->netif;

    netif_add(netif, &ipaddr, &netmask, &gw, NULL, tun_init, ip_input);
    netif_set_default(netif);
    netif_set_up(netif);
  
    netif->hwaddr_len = 6;
    netif->name[0] = 'e';
    netif->name[1] = 't';
    netif->output = low_level_output;
    netif->linkoutput = low_level_link_output;
    netif->mtu = 1500;

    sock->fd = 0;
    netif->state = sock;

    static struct udp_pcb *l_pcb;
    l_pcb = udp_new();
    if (l_pcb != NULL) {
        err_t err;

        err = udp_bind(l_pcb, IP_ADDR_ANY, htons(port));
        if (err == ERR_OK) {
          sock->lwip_pcb = l_pcb;
          udp_recv(l_pcb, tun_udp_recv, sock);
        }
        else {
          /* abort? output diagnostic? */
        }
    }
    else {
        /* abort? output diagnostic? */
    }
    return NP_OK;
}


/* lwip calls this when it has processed the incoming data
*/
void  tun_udp_recv(void *arg, struct udp_pcb *pcb, struct pbuf *p, ip_addr_t *addr, u16_t port)
{
    struct Flow *flow;
    struct Socket *asock, *lsock;
    int ret;
    struct Buffer *buf;

    lsock = (struct Socket*)arg;

    asock = apr_hash_get(HT, (void*)&port, sizeof(uint16_t));
    if (asock != NULL) {
        Log(INFO, ("accept socket found port %d", port));
        asock->state = NP_CONNECTED;
    }
    //create a fake accept socket for UDP and assign it the same fd.
    else {
        asock  = create_socket(lsock->pool, NP_TUN_UDP);
        asock->fd = lsock->fd;
        asock->fid = -1;
        asock->state = NP_ACCEPTED;
        asock->lwip_pcb = (void*)pcb;
        asock->lip = pcb->local_ip.addr;
        asock->lport = pcb->local_port;
        asock->rip = pcb->remote_ip.addr;
        asock->rport = port;
        apr_hash_set(HT, (void*)&port, sizeof(uint16_t), asock);
    }

    if (p == NULL) {
      Log(INFO, ("Tun client closed connection %d\n", 0));
      return;
    }

    buf = apr_pcalloc(asock->pool, sizeof(struct Buffer));
    buf->data = (uint8_t*)p->payload;
    buf->len = p->len;
    Log(INFO, ("read data len %d\n", buf->len));

    ret = apr_queue_trypush(asock->inQ, buf);

    if(ret == APR_EAGAIN) {
        Log(ERROR, ("asock->inQ full %d\n", apr_queue_size(asock->inQ)));
        ret = ERR_BUF;
    }
    else {
        Log(INFO, ("asock->inQ enqueued %d data len %d\n", apr_queue_size(asock->inQ), buf->len));
        //XXX need to free the pbuf if returning OK, according to docs.
        ret = ERR_OK;
    }

    if(asock->state == NP_CONNECTED) {
        flow = get_flow(asock->fid);
    }
    else if(asock->state == NP_ACCEPTED) {
        flow = forwarding(asock);
    }

    //synthetic read event on the accepted socket, similar to a real read event on
    //a tcp socket in the event loop
    process_flow(asock->fd, EV_READ, flow);

    return;
}
