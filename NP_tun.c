#include "NP.h"
#include "NP_tun.h"
#include "NP_tcp.h"
#include <linux/if.h>
#include <linux/if_tun.h>

NP_STATUS tun_input(struct Socket *s);

//CBs for lwip
err_t tun_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err);
err_t tun_sent(void *arg, struct tcp_pcb *tpcb, u16_t len);
err_t tun_init(struct netif *netif);
err_t tun_accept(void *arg, struct tcp_pcb *newpcb, err_t err);


NP_STATUS NP_tun_init(struct Socket *s)
{
    //set the FPs
    s->listen = NP_tun_listen;
    s->accept = NP_tun_accept;
    s->connect = NP_tun_connect;
    s->read = NP_tun_read;
    s->write = NP_tun_write;
    s->close = NP_tun_close;
    s->deQ = NP_tcp_deQ;
    s->enQ = NP_tcp_enQ;

    return 0;
}


struct Socket* NP_tun_accept(struct Socket *s)
{
    tun_input(s);
    return NULL;
}


NP_STATUS NP_tun_connect(struct Socket *s, NP_IPAddr ip, uint16_t port)
{
    return NP_SUCCESS;
}


int NP_tun_read(struct Socket *s, uint8_t *buf, int len)
{
    return 1;
}

int NP_tun_write(struct Socket *s, uint8_t *buft, int len)
{
    int ret, l;
    struct Buffer *buf;
    static int tlen = 0;

    //XXX TODO accumulate data if the there is no room in tcp_sndbuf
    //save the len here and since the buffers are already in the outQ
    //we dont have to worry abt that, just dont pop anything.
    
    tlen = len;

    l = tcp_sndbuf((struct tcp_pcb*)s->lwip_pcb);
    Log(DEBUG, ("sock %p can write len %d fd %d\n", s, l, s->fd));

    if (l < tlen) {
        Log(ERROR, ("sock %p cannot send any data right now, sndbuf sz %d fd %d\n", s, l, s->fd));
        return 0;
    }

    ret = apr_queue_trypop(s->outQ, (void*)&buf);

    if(ret == APR_EAGAIN) {
        Log(ERROR, ("s->outQ empty %d\n", apr_queue_size(s->outQ)));
        return 0;
    }
    else {
        Log(INFO, ("s->outQ dequeued %d\n", apr_queue_size(s->outQ)));
    }

    tcp_write(s->lwip_pcb, buf->data, buf->len, 0);
    tcp_output(s->lwip_pcb);

    return l;
}

int NP_tun_close(struct Socket *s)
{
    Log(DEBUG, ("Close tun sock %p fd %d\n", s, s->fd));
    tcp_close(s->lwip_pcb);
    return 0;
}


NP_STATUS NP_tun_listen(struct Socket *sock, NP_IPAddr ip, uint16_t port, char *intf)
{
    sock->lport = port;
    sock->lip = ip.s_addr;
    sock->interface = 0;
    sock->type = NP_LISTEN;
    sock->state = NP_LISTENING;

    struct netif *netif;
    static ip_addr_t ipaddr, netmask, gw;

    ipaddr.addr = ip.s_addr;
    gw.addr = 0;
    netmask.addr = 0;
    //IP4_ADDR(&gw, 192,168,17,254);
    //IP4_ADDR(&netmask, 255,255,255,0);

    sock->netif = apr_pcalloc(sock->pool, sizeof(struct netif));

    netif = sock->netif;

    netif_add(netif, &ipaddr, &netmask, &gw, NULL, tun_init, ip_input);
    netif_set_default(netif);
    netif_set_up(netif);
  
    netif->hwaddr_len = 6;
    netif->name[0] = 'e';
    netif->name[1] = 't';
    netif->output = low_level_output;
    netif->linkoutput = low_level_link_output;
    netif->mtu = 1500;

    sock->fd = 0;
    netif->state = sock;

    static struct tcp_pcb *l_pcb;
    l_pcb = tcp_new();
    if (l_pcb != NULL) {
        err_t err;

        err = tcp_bind(l_pcb, IP_ADDR_ANY, htons(port));
        if (err == ERR_OK) {
          l_pcb = tcp_listen(l_pcb);
          //pass on our lsock
          tcp_arg(l_pcb, sock);
          tcp_accept(l_pcb, tun_accept);
          //XXX not copying since the pcb is allocated by lwip somewhere on the heap
          //so we are using our var. as a pointer copy
          sock->lwip_pcb = l_pcb;
        }
        else {
          /* abort? output diagnostic? */
        }
    }
    else {
        /* abort? output diagnostic? */
    }
    return NP_OK;
}


err_t tun_init(struct netif *netif)
{
    return ERR_OK;
}


/* 
 * Read raw IP pkt from the tun interface and pass it to the
 * lwip input functions
 */
NP_STATUS tun_input(struct Socket *s)
{
    struct pbuf *p;
    struct netif *netif;

    Log(DEBUG, ("read event on %p fd %d\n", s, s->fd));

    netif = s->netif;

    p = low_level_input(netif);
    if (p != NULL) {
        Log(DEBUG, ("sock %p read raw pkt len %d fd %d\n", s, p->len, s->fd));
        netif->input(p, netif);
    }

    Log(DEBUG, ("sock %p read data from fd %d\n", s, s->fd));
    return NP_OK;
}


struct pbuf * low_level_input(struct netif *netif)
{
    struct pbuf *p, *q;
    int len;
    char buf[1514];
    char *bufptr;
    
    struct Socket *sock;
    sock = (struct Socket*)netif->state;

    /* Obtain the size of the packet and put it into the "len"
     * variable. 
    */
    len = read(sock->fd, buf, sizeof(buf));
    if (len < 0) {
        Log(WARNING, ("failed to read data from fd %d\n", sock->fd));
        return NULL;
    }

    // We allocate a pbuf chain of pbufs from the pool. 
    p = pbuf_alloc(PBUF_LINK, len, PBUF_POOL);

    if (p != NULL) {
      /* We iterate over the pbuf chain until we have read the entire
       * packet into the pbuf. 
      */
      bufptr = &buf[0];
      for(q = p; q != NULL; q = q->next) {
          /* Read enough bytes to fill this pbuf in the chain. The
           * available data in the pbuf is given by the q->len
           * variable.
          */
          // read data into(q->payload, q->len);
          memcpy(q->payload, bufptr, q->len);
          bufptr += q->len;
      }
      /* acknowledge that packet has been read(); */
    } 
    else {
        /* drop packet(); */
        printf("Could not allocate pbufs\n");
    }

    return p;
}

err_t low_level_link_output(struct netif *netif, struct pbuf *p)
{
    return ERR_OK;
}

err_t low_level_output(struct netif *netif, struct pbuf *p, ip_addr_t *ip)
{
    struct Socket *sock;
    struct pbuf *q;
    char buf[1514];
    char *bufptr;
    int written;

    sock = (struct Socket*)netif->state;

    /* initiate transfer(); */

    bufptr = &buf[0];

    for(q = p; q != NULL; q = q->next) {
        /* Send the data from the pbuf to the interface, one pbuf at a
         * time. The size of the data in each pbuf is kept in the ->len
         * variable. 
        */

        // send data from(q->payload, q->len);
        memcpy(bufptr, q->payload, q->len);
        bufptr += q->len;
    }

    /* signal that packet should be sent(); */
    written = write(sock->fd, buf, p->tot_len);
    if (written == -1) {
        perror("tapif: write");
    }

    return ERR_OK;
}


/* lwip calls this when it has processed the incoming data
*/
err_t tun_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err)
{
    struct Flow *flow;
    int ret;
    struct Buffer *buf;

    flow = (struct Flow*)arg;

    if (p == NULL) {
      Log(INFO, ("Tun client closed connection %d\n", 0));
      return ERR_CONN;
    }

    buf = apr_pcalloc(flow->sock1->pool, sizeof(struct Buffer));
    buf->data = (uint8_t*)p->payload;
    buf->len = p->len;

    ret = apr_queue_trypush(flow->sock1->inQ, buf);

    if(ret == APR_EAGAIN) {
        Log(ERROR, ("sock1->inQ full %d\n", apr_queue_size(flow->sock1->inQ)));
        ret = ERR_BUF;
    }
    else {
        Log(INFO, ("sock1->inQ enqueued %d\n", apr_queue_size(flow->sock1->inQ)));
        //XXX need to free the pbuf if returning OK, according to docs.
        ret = ERR_OK;
    }

    //tcp_recved(flow->sock1->lwip_pcb, buf->len);
    tcp_recved(tpcb, buf->len);

    //synthetic read event on the accepted socket, similar to a real read event on
    //a tcp socket in the event loop
    process_flow(flow->sock1->fd, EV_READ, flow);

    return ret;
}


err_t tun_accept(void *arg, struct tcp_pcb *newpcb, err_t err)
{
    struct Socket *asock;
    struct Flow *flow;
    struct Socket *lsock;

    lsock = (struct Socket*)arg;

    asock = create_socket(lsock->pool, NP_TUN_TCP);
    asock->addr = newpcb;
    asock->lwip_pcb = newpcb;
    asock->state = NP_ACCEPTED;
    asock->lip = newpcb->local_ip.addr;
    asock->lport = newpcb->local_port;
    asock->rip = newpcb->remote_ip.addr;
    asock->rport = newpcb->remote_port;

    //make the fd of the accepted socket same as listen fd, since from
    //the linux side of things that is the fd that the all pkts on tun 
    //come from, unlike a glibc tcp accept where we get a new fd.
    asock->fd = lsock->fd;

    flow = forwarding(asock);
    if (flow == NULL) {
        Log(ERROR, ("Cannot continue with this connection, abort fd %d\n", asock->fd));
        tcp_close(newpcb);
        return ERR_CONN;
    }

    tcp_arg(newpcb, flow);
    tcp_recv(newpcb, tun_recv);
    struct tcp_pcb *tpcb = (struct tcp_pcb*)lsock->lwip_pcb;
    tcp_accepted(tpcb);
    tcp_sent(newpcb, tun_sent);
    return ERR_OK;
}


err_t tun_sent(void *arg, struct tcp_pcb *tpcb, u16_t len)
{
    return ERR_OK;
}
