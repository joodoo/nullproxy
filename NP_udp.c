#include "NP_udp.h"
#include "NP_tcp.h"

NP_STATUS NP_udp_init(struct Socket *s)
{
    //set the FPs
    s->listen = NP_udp_listen;
    s->accept = NP_udp_accept;
    s->connect = NP_udp_connect;
    s->read = NP_udp_read;
    s->write = NP_udp_write;
    s->close = NP_udp_close;
    s->deQ = NP_tcp_deQ;
    s->enQ = NP_tcp_enQ;

    return 0;
}

NP_STATUS NP_udp_listen(struct Socket *sock, NP_IPAddr ip, uint16_t port, char *intf)
{
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = port;
    addr.sin_addr.s_addr = ip.s_addr;

    //memcpy(sock->addr, &addr, sizeof(addr));
    //sock->addrlen = sizeof(addr);

    sock->lport = port;
    sock->lip = ip.s_addr;

    if(intf != NULL) {
        sock->interface = intf_index(sock->fd, intf);
    }

    sock->type = NP_LISTEN;
    sock->state = NP_LISTENING;

    sock->fd = socket(PF_INET, SOCK_DGRAM, 0);

    if (bind(sock->fd, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) == -1) {
        perror("bind failed");
        return NP_FAIL;
    }

    Log(INFO, ("Created UDP LISTEN socket %p fd %d\n", sock, sock->fd));

    /*
    s->fid = new_flow();
    flow = get_flow(s->fid);
    flow->sock1 = s;
    */

    return NP_OK;
}

struct Socket* NP_udp_accept(struct Socket *lsock)
{
    struct sockaddr_in remote;
    socklen_t rlen = sizeof(remote);
    struct Socket *asock;
    int ret;

    struct Buffer *buf;

    buf = apr_pcalloc(lsock->pool, sizeof(struct Buffer));
    buf->data = apr_pcalloc(lsock->pool, 4096);

    buf->len = recvfrom(lsock->fd, buf->data, 4096, 0, (struct sockaddr *)&remote, &rlen);

    //if a socket exists in hash with src port as the key, then this is a
    //data pkt and not a new UDP "connection" so return the existing accept sock
    asock = apr_hash_get(HT, (void*)&remote.sin_port, sizeof(uint16_t));
    if (asock != NULL) {
        Log(INFO, ("accept socket found port %d", remote.sin_port));
        asock->state = NP_CONNECTED;
    }
    //create a fake accept socket for UDP and assign it the same fd.
    else {
        asock  = create_socket(lsock->pool, NP_UDP);
        asock->fd = lsock->fd;
        asock->fid = -1;
        asock->state = NP_ACCEPTED;
        apr_hash_set(HT, (void*)&remote.sin_port, sizeof(uint16_t), asock);
    }

    asock->rip = remote.sin_addr.s_addr;
    asock->rport = remote.sin_port;

    ret = apr_queue_trypush(asock->inQ, buf);

    if(ret == APR_EAGAIN) {
        Log(ERROR, ("asock->inQ full %d\n", apr_queue_size(asock->inQ)));
        return 0;
    }
    else {
        Log(INFO, ("asock->inQ enqueued %d\n", apr_queue_size(asock->inQ)));
    }

    return asock;
}

NP_STATUS NP_udp_connect(struct Socket *s, NP_IPAddr ip, uint16_t port)
{
    s->rip = ip.s_addr;
    s->rport = port;

    s->fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    Log(INFO, ("Setting socket %d to non-blocking\n", s->fd));
    int flags = fcntl(s->fd, F_GETFL, 0);
    fcntl(s->fd, F_SETFL, flags | O_NONBLOCK);

    Log(INFO, ("Binding socket %d to %s\n", s->fd, s->intf_name));
    if (protectFD(s->fd) < 0) {
        Log(ERROR, ("protectFD failed for fd %d\n", s->fd));
        return -1;
    }

    return NP_OK;
}

int NP_udp_read(struct Socket *s, uint8_t *buft, int len)
{
    int ret;
    struct Buffer *buf;
    struct sockaddr_in from;
    socklen_t flen = sizeof(from);

    buf = apr_pcalloc(s->pool, sizeof(struct Buffer));
    buf->data = apr_pcalloc(s->pool, 4096);

    buf->len = recvfrom(s->fd, buf->data, 4096, 0, (struct sockaddr *)&from, &flen);
    ret = apr_queue_trypush(s->inQ, buf);
    if(ret == APR_EAGAIN) {
        Log(ERROR, ("s->inQ full %d\n", apr_queue_size(s->inQ)));
        return 0;
    }
    else {
        Log(INFO, ("s->inQ enqueued %d\n", apr_queue_size(s->inQ)));
    }

    Log(DEBUG, ("sock %p read len %d fd %d\n", s, buf->len, s->fd));
    return buf->len;
}

int NP_udp_write(struct Socket *s, uint8_t *buft, int len)
{
    int ret, l;
    struct Buffer *buf;
    ret = apr_queue_trypop(s->outQ, (void*)&buf);

    if(ret == APR_EAGAIN) {
        Log(ERROR, ("s->outQ empty %d\n", apr_queue_size(s->outQ)));
        return 0;
    }
    else {
        Log(INFO, ("s->outQ dequeued %d\n", apr_queue_size(s->outQ)));
    }

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = s->rport;
    addr.sin_addr.s_addr = s->rip;

    Log(DEBUG, ("NP_udp_write: sock %p fd %d\n", s, s->fd));
    l = sendto(s->fd, buf->data, buf->len, 0, (struct sockaddr*)&addr, (socklen_t)sizeof(addr));
    if (l < 0) {
        perror("sendto");
        Log(ERROR, ("failed to sendto sock %p fd %d\n", s, s->fd));
    }

    return l;
}

int NP_udp_close(struct Socket *s)
{
    return 0;
}
