package com.example.nullproxy;

import java.util.ArrayList;

import com.example.nullproxy.R;

import android.net.VpnService;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.view.Menu;
import android.view.View;
import android.util.Log;

public class MainActivity extends Activity implements View.OnClickListener {

	Messenger mService = null;
    final Messenger mMessenger = new Messenger(new IncomingHandler());
	private String mTAG = "Main";
	private String mTunIP = "192.168.200.200";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        findViewById(R.id.start).setOnClickListener(this);
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	
	@Override
	public void onClick(View v) {
		Log.i("", "Start clicked");
		//Intent i = new Intent(this, SampleService.class);
		//startService(i);
		
		Intent intent = VpnService.prepare(this);
		if (intent != null) {
			startActivityForResult(intent, 0);
		} 
    else {
			onActivityResult(0, RESULT_OK, null);
		}	
		
	}
	
	
	@Override
    protected void onActivityResult(int request, int result, Intent data) {
        if (result == RESULT_OK) {
            Intent i = new Intent(this, NullProxyService.class);
            Bundle b = new Bundle();
            b.putInt("fd", Integer.valueOf(0));
            b.putString("addr", mTunIP);
            
            Bundle r1 = new Bundle();
            r1.putString("route", "1.2.3.0");
            r1.putInt("prefix", 24);
            Bundle r2 = new Bundle();
            r2.putString("route", "2.3.4.0");
            r2.putInt("prefix", 16);
            ArrayList<Bundle> r = new ArrayList<Bundle>();
            r.add(r1);
            r.add(r2);
            
            b.putString("route", "8.8.8.0");
            b.putString("dns", "4.2.2.1");
            
            i.putExtras(b);
            
            startService(i);    	
            doBindService();
        }
    }
	
	
    public void onClickStop(View v) {
    	Log.i("", "Stop clicked");
    	//Intent i = new Intent(this, NullProxyService.class);
		//stopService(i);
		try {
            Message msg = Message.obtain(null, 0);
            Bundle b = new Bundle();
            b.putInt("stop", 1);
            msg.setData(b);
            mService.send(msg);
        } 
        catch (RemoteException e) {
        	Log.e(mTAG, "Message send to service failed");
        }
    }
    
    
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);

            try {
                Message msg = Message.obtain(null, 0);
                //no data to send, give the service the messenger handle to send 
                //back the messages on.
                msg.replyTo = mMessenger;
                mService.send(msg);
            } 
            catch (RemoteException e) {
            	Log.e(mTAG, "Message send to service failed");
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mService = null;
        }
    };
    
    
    //Handler to recv message from VPNService
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            handleMsg(msg);  		
        }
    }
    
    void handleMsg(Message msg) {
    	Bundle b = msg.getData();
        Integer fd = b.getInt("fd");
    	Log.i(mTAG, " fd " + fd); 
    }
    
    void doBindService() {
        bindService(new Intent(this, NullProxyService.class), mConnection, Context.BIND_AUTO_CREATE);
    }
}
