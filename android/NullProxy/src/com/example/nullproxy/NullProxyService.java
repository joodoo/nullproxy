
package com.example.nullproxy;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.VpnService;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

public class NullProxyService extends VpnService {
	int mStartMode;       // indicates how to behave if the service is killed
	IBinder mBinder;      // interface for clients that bind
	boolean mAllowRebind; // indicates whether onRebind should be usef
	private String mTAG = "NullProxyService";
	private enum State {INIT, START, STOP};
	private State mState;
	private ParcelFileDescriptor mInterface;
	private int mVpnFd;
	private int mNullProxyStatus;
    private PendingIntent mConfigureIntent;
    Bundle mConfig;
    final Messenger mMessenger = new Messenger(new IncomingHandler());
    Messenger mReplyMessenger;
    
	@Override
	public void onCreate() {
		// The service is being created
		getApplicationContext();
		Log.i(mTAG, "onCreate: NullProxyService ");
		mState = State.INIT;
	}


	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (mState == State.START) {
			mNullProxyStatus = nullProxyGetStatus();
			Log.i(mTAG, "onStartCommand: NullProxyService already started, status " + 
								mNullProxyStatus);
			return START_NOT_STICKY;
		}

		mState = State.START;
		// The service is starting, due to a call to startService()
		mConfig = intent.getExtras();    	
		//setup the VPN interface 
		try {
			configure();
		} 
		catch (Exception e) {
			Log.i(mTAG, "configure VPN service failed");
		}

		Log.i(mTAG, " onStartCommand fd " + mVpnFd);
		nullProxyStart(mVpnFd);

		mNullProxyStatus = nullProxyGetStatus();
		Log.i(mTAG, " nullproxy status " + mNullProxyStatus);

		return START_NOT_STICKY;
	}


	@Override
	public IBinder onBind(Intent intent) {
	    return mMessenger.getBinder();
	}


	@Override
	public boolean onUnbind(Intent intent) {
		// All clients have unbound with unbindService()
		return mAllowRebind;
	}


	@Override
	public void onRebind(Intent intent) {
		// A client is binding to the service with bindService(),
		// after onUnbind() has already been called
	}


	@Override
	public void onDestroy() {
		if (mState == State.STOP || mState == State.INIT) {
			Log.i(mTAG, "onStartCommand: NullProxyService already stopped");
			return;
		}
		
		Log.i(mTAG, "stop service ");
		Toast.makeText(this, "service stopping", Toast.LENGTH_LONG).show();
		try {
			mInterface.close();
		}
		catch (Exception e) {
			Log.i(mTAG, "VPN interface close: failed");
		}
		mState = State.STOP;
		nullProxyStop();
	}

	
	private void configure() throws Exception {
		// If the old interface has exactly the same parameters, use it!
		if (mInterface != null) {
			Log.i(mTAG, "Using the previous interface");
			return;
		}

		Builder builder = new Builder();
		try {
			// Configure a builder while parsing the parameters.
			builder.setMtu(1500);
			builder.addAddress(mConfig.getString("addr"), 32);
			builder.addRoute(mConfig.getString("route"), 24);
			builder.addDnsServer(mConfig.getString("dns"));
		}
		catch (Exception e) {
			throw new IllegalArgumentException("Bad parameter: ");
		}
    
        
        // Close the old interface since the parameters have been changed.
		try {
			mInterface.close();
		} 
		catch (Exception e) {
			// ignore
		}

		try {
			// Create a new interface using the builder and save the parameters.
			mInterface = builder.setSession("")
					.setConfigureIntent(mConfigureIntent)
					.establish();
			Log.i(mTAG, "New interface: " + mInterface + " fd " + mInterface.getFd());
			mVpnFd = mInterface.getFd();
			//mInterface.detachFd();
		}
		catch (Exception e) {
			Log.i(mTAG, "New interface: failed");
		}
	}

	
	//handler to recv messages from MainActivity
    class IncomingHandler extends Handler { // Handler of incoming messages from clients.
        @Override
        public void handleMessage(Message msg) {
        	super.handleMessage(msg);
        	handleMsg(msg);
        }
    }
    
    void handleMsg(Message msg) {
    	Bundle b = msg.getData();
    	int s = b.getInt("stop");
    	if (s == 1) {
    		//stopSelf();
    		onDestroy();
    		return;
    	}
     	
    	//set the handle back to the activity.
    	mReplyMessenger = msg.replyTo;
    	
    	if(mReplyMessenger != null && mInterface != null) {
    		
        	//send the fd back to the activity to pass on to the ProxyService
			Message rmsg = Message.obtain(null, 0);
            Bundle br = new Bundle();
            br.putInt("fd", 0);
            rmsg.setData(br);
            try {
            	mReplyMessenger.send(rmsg);
            }
            catch (RemoteException e) {
            	Log.e(mTAG, "Message send to activity failed");
            }
		}
    }

    
    public int protectFD(int fd) {
		Log.i(mTAG, "protect fd " + fd + " from VPN");
    	boolean r = protect(fd);
    	if (r) {
    		return 1;
    	}
    	else {
    		return 0;
    	}
    }
    

	static {
		System.loadLibrary("nullproxy");
	}
	
	
	//JNI interface to the library	
	public native int nullProxyGetStatus();
	public native int nullProxyStart(int fd);
	public native int nullProxyStop();
}