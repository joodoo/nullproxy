package com.example.nullproxy;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

public class SampleService extends Service {
	int mStartMode;       // indicates how to behave if the service is killed
	IBinder mBinder;      // interface for clients that bind
	boolean mAllowRebind; // indicates whether onRebind should be used
	private String mTAG = "SampleService";
	private TelephonyManager tMgr;
	private int mNetworkType = 0;


	@Override
	public void onCreate() {
		// The service is being created
		getApplicationContext();
		tMgr = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
		mNetworkType = tMgr.getNetworkType();

		Log.i(mTAG, "OnCreate: SampleService network_type " + mNetworkType + " msg: " + message("junk"));
		Toast.makeText(this, "service starting", Toast.LENGTH_LONG).show();
		sampleStart();
	}


	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// The service is starting, due to a call to startService()
		int c = sampleGetCount();
		Log.i(mTAG, " onStartCommand count " + c);
		Log.i(mTAG, " onStartCommand count " + c + " " + stringFromSampleJNI());
		Toast.makeText(this, "Sample Count " + c, Toast.LENGTH_LONG).show();

		return START_STICKY;
	}


	@Override
	public IBinder onBind(Intent intent) {
		// A client is binding to the service with bindService()
		return mBinder;
	}


	@Override
	public boolean onUnbind(Intent intent) {
		// All clients have unbound with unbindService()
		return mAllowRebind;
	}


	@Override
	public void onRebind(Intent intent) {
		// A client is binding to the service with bindService(),
		// after onUnbind() has already been called
	}


	@Override
	public void onDestroy() {
		// The service is no longer used and is being destroyed
		Log.i(mTAG, "stop service ");
		Toast.makeText(this, "service stopping", Toast.LENGTH_LONG).show();
		sampleStop();
	}


    public String message(String text) {
		Log.i(mTAG, "message called");
    	text = text + " from java";
    	return text;
    }
    
	static {
		System.loadLibrary("sample");
	}
	public native int sampleGetCount();
	public native int sampleStart();
	public native int sampleStop();
	public native String stringFromSampleJNI();

}