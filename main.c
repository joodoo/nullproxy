#include "NP.h"
#include <pthread.h>
#include "NP_config.h"

int nullProxyStop();

#ifdef _linux_
int nullProxyStart(char *ip, int fd)
{
    struct TunInterface *ti;
    ti = (struct TunInterface*)malloc(sizeof(struct TunInterface));
    ti->ip = (char*)calloc(32, sizeof(char));
    strcpy(ti->ip, ip);
    ti->fd = fd;

    fp = fopen("/tmp/nullproxy-init.txt", "w");

    pthread_t sthread;
    pthread_create(&sthread, NULL, NP_init, (void*)ti);
    pthread_join(sthread, NULL);
    return 0;
}

int linux_nullProxyStop()
{
    nullProxyStop();
    return 0;
}

int protectFD(int fd)
{
    struct ifreq ifr;
    memset(&ifr, 0, sizeof(ifr));
    snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", NP_CFG->out_intf);
    if (setsockopt(fd, SOL_SOCKET, SO_BINDTODEVICE, (void *)&ifr, sizeof(ifr)) < 0) {
    /*
    if (setsockopt(fd, SOL_SOCKET, SO_BINDTODEVICE, NP_CFG->out_intf, sizeof(NP_CFG->out_intf)) < 0) {
    */
        Log(ERROR, ("BINDTODEVICE failed errno %d\n", errno));
        return -1;
    }
    return 0;
}


#endif


#ifdef _android_
#include <jni.h>

//globals for protectFD call
static jobject g_obj;
static jmethodID g_mid;
static JavaVM *gJavaVM;

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    static JNIEnv *env;
    //cache the vm
    gJavaVM = vm;
    fp = fopen("/sdcard/np.txt", "w");
    if ((*gJavaVM)->GetEnv(gJavaVM, (void**) &env, JNI_VERSION_1_6) != JNI_OK) {
			fprintf(fp, "Failed to get the environment using GetEnv()");
			return -1;
    }
    fprintf(fp, "JNI_OnLoad called, env %p\n", env);
    fflush(fp);
    return JNI_VERSION_1_6;
}
    

void *NP_init_wrapper(void *arg)
{
    static JNIEnv *env;
    fprintf(fp, "NP_init_wrapper starting NP_init \n");
    fflush(fp);

    int status = (*gJavaVM)->AttachCurrentThread(gJavaVM, &env, NULL);
    if(status < 0) {
        fprintf(fp, "NP_init: thread failed to attach %d", status);
        fflush(fp);
        return 0;
    }

    NP_init(arg);
    return 0;
}


jint Java_com_example_nullproxy_NullProxyService_nullProxyStart(JNIEnv* env, jobject obj, jint fd)
{
    struct TunInterface *ti;
    ti = (struct TunInterface*)malloc(sizeof(struct TunInterface));
    ti->ip = (char*)calloc(32, sizeof(char));

    //this is an internal tunnel IP, can be fixed
    strcpy(ti->ip, "192.168.200.200");
    ti->fd = fd;

    fprintf(fp, "nullProxyStart start NP_init ip %s %d\n", ti->ip, fd);
    fflush(fp);

    g_obj = (*env)->NewGlobalRef(env, obj);

    jclass cls = (*env)->FindClass(env, "com/example/nullproxy/NullProxyService");
    g_mid = (*env)->GetMethodID(env, cls, "protectFD", "(I)I");
    if (g_mid == 0) {
        return 0;
    }

    nullProxyStatus = 1;

    pthread_t sthread;
    pthread_create(&sthread, NULL, NP_init_wrapper, (void*)ti);
    return 0;
}

jint Java_com_example_nullproxy_NullProxyService_nullProxyStop(JNIEnv* env, jobject obj)
{
    return nullProxyStop();
}

jint Java_com_example_nullproxy_NullProxyService_nullProxyGetStatus(JNIEnv* env, jobject obj)
{
    return nullProxyStatus;
}

int protectFD(int fd)
{
    static JNIEnv *env;

    if ((*gJavaVM)->GetEnv(gJavaVM, (void**) &env, JNI_VERSION_1_6) != JNI_OK) {
        Log(ERROR, ("Failed to get the environment using GetEnv() %p", env));
        return 0;
    }

    jboolean res = (int)(*env)->CallObjectMethod(env, g_obj, g_mid, (jint)fd);
    Log(INFO, ("protect status: %d\n", (int)res));

    return (int)res;
}

#endif

int nullProxyStop()
{
    fprintf(fp, "nullProxyStop \n");
    fflush(fp);
    fclose(fp);
    nullProxyStatus = 0;
    exit(0);
}
