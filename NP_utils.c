/*
 * Compile with:
 * gcc -I install_d/usr/local/include -g3 -o nullproxy nullproxy.c -L. -levent -L. -lhash
 *  * 03/09/2011 - status
 *      raw/udp <==> udp/raw works
 *      raw X raw works
 *      no flow table created
 *
 */

#include "NP_utils.h"
#include "NP_tcp.h"
#include "NP_udp.h"
#include "NP_tun.h"
#include "NP_tun_udp.h"
#include "NP_config.h"

//static char* SocketProtoStr[] = {"NP_TCP", "NP_UDP", "NP_RAW", "NP_NULL"};
//static char* SocketTypeStr[] = {"NP_LISTEN", "NP_CONNECT", "NP_ACCEPT", "NP_NOOP"};

struct Socket* create_socket(apr_pool_t *pool, SocketProto proto)
{
    struct Socket *sock;

    sock = apr_pcalloc(pool, sizeof(struct Socket));
    apr_pool_create(&sock->pool, pool);
    sock->ev = apr_pcalloc(sock->pool, sizeof(struct event));
    sock->addr  = apr_pcalloc(sock->pool, sizeof(struct sockaddr_in));
    sock->addrlen = sizeof(struct sockaddr_in);

    sock->fd = 0;
    sock->lip = 0;
    sock->lport = 0;
    sock->rip = 0;
    sock->rport = 0;
    sock->proto = NP_NULL;
    sock->type = NP_NOOP;
    sock->interface = 0;
    sock->state = NP_NOOPSTATE;
    apr_queue_create(&sock->inQ, 100, sock->pool);
    apr_queue_create(&sock->outQ, 100, sock->pool);

    sock->proto = proto;
    if (proto == NP_TCP) {
        Log(INFO, ("Created TCP base socket %p\n", sock));
        sock->init = NP_tcp_init;
    }
    else if (proto == NP_UDP) {
        Log(INFO, ("Created UDP base socket %p\n", sock));
        sock->init = NP_udp_init;
    }
    else if (proto == NP_TUN_TCP) {
        Log(INFO, ("Created TUN base socket %p\n", sock));
        sock->init = NP_tun_init;
    }
    else if (proto == NP_TUN_UDP) {
        Log(INFO, ("Created TUN base socket %p\n", sock));
        sock->init = NP_tun_udp_init;
    }
    else if (proto == NP_RAW) {
    }
    else if (proto == NP_NULL) {
        Log(INFO, ("Created empty socket %p\n", sock));
    }

    sock->init(sock);
    return sock;
}


int intf_index(int fd, char *interface)
{
    struct ifreq ifr;

    strncpy(ifr.ifr_name, interface, sizeof(ifr.ifr_name));
    if (ioctl(fd, SIOCGIFINDEX, &ifr) == -1)
    {
        perror("SIOCGIFINDEX failed");
        return -1;
    }
    fprintf(stderr, "interface %s index = %d\n", interface, ifr.ifr_ifindex);
    return ifr.ifr_ifindex;
}

NP_IPAddr NP_inet_addr(char *ip)
{
  struct in_addr ipa;
  ipa.s_addr = inet_addr(ip);
  return ipa;
}


NP_Port NP_htons(uint16_t port)
{
  return htons(port);
}

char * NP_inet_ntoa(NP_IPAddr ip)
{
  return inet_ntoa(ip);
}

static FILE *logfp;

void NP_log_init()
{
    LOGLEVEL = NP_CFG->loglevel;
    logfp = fopen(NP_CFG->logfile, "w");
}

void NP_log_printf(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  time_t now = time(0);
  struct tm * timeinfo = localtime(&now);
  char *timestr = asctime(timeinfo);
  timestr[strlen(timestr)-1] = '\0';
  fprintf(logfp, "[%s] ", timestr);
  vfprintf(logfp, fmt, ap);
  fflush(logfp);
  va_end(ap);
}
