#ifndef __NP_FLOW_H__
#define __NP_FLOW_H__

#include "NP.h"
#include <assert.h>
#include <time.h>

#define MAX_FLOWS 20

NP_STATUS init_flows();

struct Flow* get_flow(int id);
int new_flow();

void free_flow();

void test_flows();

#endif
