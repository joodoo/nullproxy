#include <stdio.h>
#include <stdlib.h>
#include <jni.h>
#include <pthread.h> 
#include <unistd.h>

int count = 0;
FILE *fpa;
void *sample_counter();
static jobject g_obj;
static jmethodID g_mid;
static JavaVM *gJavaVM;

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    static JNIEnv *env;
    //cache the vm
    gJavaVM = vm;
    fpa = fopen("/sdcard/sample-log.txt", "a");
    if ((*gJavaVM)->GetEnv(gJavaVM, (void**) &env, JNI_VERSION_1_6) != JNI_OK) {
        fprintf(fpa, "Failed to get the environment using GetEnv()");
        return -1;
    }
    fprintf(fpa, "JNI_OnLoad called, env %p\n", env);
    fflush(fpa);
    return JNI_VERSION_1_6;
}

jint Java_com_example_nullproxy_SampleService_sampleStart(JNIEnv* env, jobject obj)
{

    g_obj = (*env)->NewGlobalRef(env, obj); 

    jclass cls = (*env)->FindClass(env, "com/example/nullproxy/SampleService");
    g_mid = (*env)->GetMethodID(env, cls, "message", "(Ljava/lang/String;)Ljava/lang/String;");
    if (g_mid == 0) { 
        return 0; 
    } 

    fprintf(fpa, "sampleStart %d g_obj %p, g_mid %p\n", count, g_obj, g_mid);

    pthread_t sthread; 
    pthread_create(&sthread, NULL, sample_counter, NULL);
    return 0;
}


jint Java_com_example_nullproxy_SampleService_sampleStop(JNIEnv* env, jobject obj)
{
    fprintf(fpa, "sampleStop %d\n", count);
    exit(0);
}


void *sample_counter()
{
    static JNIEnv *env;
    int status = (*gJavaVM)->AttachCurrentThread(gJavaVM, &env, NULL);
    if(status < 0) {
        fprintf(fpa, "sample_counter: thread failed to attach ");
        return 0;
    }

    if ((*gJavaVM)->GetEnv(gJavaVM, (void**) &env, JNI_VERSION_1_6) != JNI_OK) {
        fprintf(fpa, "Failed to get the environment using GetEnv()");
        return 0;
    }

    fprintf(fpa, "sample counter, env %p\n", env);
    fflush(fpa);

    while (1) {
        count++;
        fprintf(fpa, "thr 1 sample count %d\n", count);
        fflush(fpa);
        sleep(5);
        if (count % 4 == 0) {
          fprintf(fpa, "sample_counter: count reached invoking jni method message\n");
          fflush(fpa);
          jstring jstr = (*env)->NewStringUTF(env, "From sample_counter ");
          jstring result = (*env)->CallObjectMethod(env, g_obj, g_mid, jstr);
          const char *cstr = (*env)->GetStringUTFChars(env, result, 0);
          fprintf(fpa, "sample counter: message from java: %s %p\n", cstr, env);
        }
    }
    return 0;
}


jint Java_com_example_nullproxy_SampleService_sampleGetCount(JNIEnv* env, jobject obj)
{
    fprintf(fpa, "sampleGetCOunt: %d %p\n", count, env);
    return count;
}


jstring Java_com_example_nullproxy_SampleService_stringFromSampleJNI(JNIEnv* env, jobject obj)
{
    jstring jstr = (*env)->NewStringUTF(env, "From stringFromSampleJNI ");
    jstring result = (*env)->CallObjectMethod(env, g_obj, g_mid, jstr);
    const char *cstr = (*env)->GetStringUTFChars(env, result, 0);
    fprintf(fpa, "message from java: %s %p\n", cstr, env);

    return (*env)->NewStringUTF(env, "Hello from SampleJNI !");
}


int main()
{
    Java_com_example_nullproxy_SampleService_sampleStart(NULL, NULL);
    return 0;
}

