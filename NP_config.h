#ifndef __NP_CONFIG_H__
#define __NP_CONFIG_H__

struct Proxy {
  char ip[16];
  short port;
};

struct Rule {
  char src[16];
  short sport;
  char dest[16];
  short dport;
  char proto[8];
  char target[32];
  struct Proxy proxies[2];
};

struct Config {
  struct Rule rules[10];
  int rule_cnt;
  char out_intf[8];
  int loglevel;
  char logfile[32];
};

void load_config2(void);
void print_config(void);

struct Config *NP_CFG;

#endif
