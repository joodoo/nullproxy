#include "NP.h"
#include "NP_config.h"
#include "parson.h"

/* Testing correctness of parsed values */
void load_config2(void) 
{
    JSON_Value *root_value;
    JSON_Object *object;
    JSON_Object *obj1, *obj2;
    const char *tmp;

    JSON_Array *array, *proxies;

    //NP_CFG = apr_pcalloc(mainpool, sizeof(struct Config));
    NP_CFG = calloc(1, sizeof(struct Config));

    int i, j, k;
    const char *filename = "nullproxy.conf";
    printf("Testing %s:\n", filename);
    root_value = json_parse_file(filename);
    object = json_value_get_object(root_value);
    const char *s = json_object_get_string(object, "out_intf");
    printf("out_intf: %s\n", s);
    strcpy(NP_CFG->out_intf, s);

    s = json_object_get_string(object, "logfile");
    if (s) strcpy(NP_CFG->logfile, s);

    int level = json_object_get_number(object, "loglevel");
    NP_CFG->loglevel = level;

    array = json_object_get_array(object, "rules");
    if (array != NULL) {
        NP_CFG->rule_cnt = json_array_get_count(array);
        for (i = 0; i < json_array_get_count(array); i++) {
            obj1 = json_array_get_object(array, i);
            j = json_object_get_number(obj1, "dport");

            tmp = json_object_get_string(obj1, "src");
            if (tmp) {
              strcpy(NP_CFG->rules[i].src, tmp);
            }

            tmp = json_object_get_string(obj1, "dest");
            if (tmp) {
              strcpy(NP_CFG->rules[i].dest, tmp);
            }

            tmp = json_object_get_string(obj1, "proto");
            if (tmp) {
              strcpy(NP_CFG->rules[i].proto, tmp);
            }

            NP_CFG->rules[i].sport = json_object_get_number(obj1, "sport");
            NP_CFG->rules[i].dport = json_object_get_number(obj1, "dport");

            tmp = json_object_get_string(obj1, "target");
            if (tmp) {
              strcpy(NP_CFG->rules[i].target, tmp);
            }

            printf("rule %d: src %s:%d, dest %s:%d, proto %s, target %s\n", i,
                NP_CFG->rules[i].src, NP_CFG->rules[i].sport,
                NP_CFG->rules[i].dest, NP_CFG->rules[i].dport,
                NP_CFG->rules[i].proto, NP_CFG->rules[i].target);

            proxies = json_object_get_array(obj1, "proxies");
            for (k = 0; k < json_array_get_count(proxies); k++) {
                obj2 = json_array_get_object(proxies, k);
                j = json_object_get_number(obj2, "port");
                printf("\tproxy %d: %s:%d\n", k, json_object_get_string(obj2, "ip"), j);

                tmp = json_object_get_string(obj2, "ip");
                if (tmp) strcpy(NP_CFG->rules[i].proxies[k].ip, tmp);

                NP_CFG->rules[i].proxies[k].port = json_object_get_number(obj2, "port");
                if (tmp) strcpy(NP_CFG->rules[i].proxies[k].ip, tmp);
            }
        }
    }
    json_value_free(root_value);
}

void print_config()
{
    return;
}
